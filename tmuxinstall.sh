#!/bin/bash

echo "Installing dev tools..."
xcode-select --install

echo "Downloading tmux & libevent"
curl -OLs https://github.com/tmux/tmux/releases/download/2.6/tmux-2.6.tar.gz
curl -OLs https://github.com/libevent/libevent/releases/download/release-2.1.8-stable/libevent-2.1.8-stable.tar.gz
tar -xvf tmux-2.6.tar.gz 
tar -xvf libevent-2.1.8-stable.tar.gz 

echo "Installing libevent"
cd ./libevent-2.1.8-stable
./configure
make
make install

echo "Installing tmux"
cd ../tmux-2.6
./configure
make
make install

echo "Tmux 2.6 installed"
tmux -V

echo "Cleaning up..."
#cd ../
rm -rf ./libevent-2.1.8-stable
rm -rf ./tmux-2.6
rm tmux-2.6.tar.gz
rm libevent-2.1.8-stable.tar.gz

cp ./tmuxconfig.conf ~/.config
echo alias tmux=\"tmux -f /Users/$(whoami)/.config/tmuxconfig.conf\" >> ~/.zshrc
